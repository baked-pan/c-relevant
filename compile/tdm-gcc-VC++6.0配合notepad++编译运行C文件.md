---
title: tdm-gcc/VC++6.0配合notepad++编译运行C文件 
tags: 编译 ,c程序
grammar_cjkRuby: true
---
[TOC]


***

## 1.环境变量
```
%ALLUSERSPROFILE% 局部 返回所有“用户配置文件”的位置。
%APPDATA% 局部 返回默认情况下应用程序存储数据的位置。
%CD% 局部 返回当前目录字符串。
%CMDCMDLINE% 局部 返回用来启动当前的 Cmd.exe 的准确命令行。
%CMDEXTVERSION% 系统 返回当前的“命令处理程序扩展”的版本号。
%COMSPEC% 系统 返回命令行解释器可执行程序的准确路径。
%DATE% 系统 返回当前日期。使用与 date /t 命令相同的式。由 Cmd.exe生成。有关 date 命令的详细信息，请参阅 Date。
%ERRORLEVEL% 系统 返回最近使用过的命令的错误代码。通常用非零表示错误。
%HOMEDRIVE% 系统 返回连接到用户主目录的本地工作站驱动器号。基于主目录的设置。用户主目录是在“本地用户和组”中指定的。
%HOMEPATH% 系统 返回用户主目录的完整路径。基于主目录的设置。用户主目录是在“本地用户和组”中指定的
```
***
## 2.基本命令行
### (1)**VC++6.0**
#### 【编译】
`cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl/c "$(FILE_NAME)" & PAUSE & EXIT`
#### 【编译+LINK】
`cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl "$(FILE_NAME)" & PAUSE & EXIT`
#### 【RUN】
`cmd /k chdir /d "$(CURRENT_DIRECTORY)" & echo Running: & "$(NAME_PART).exe" & PAUSE & EXIT`
#### 【编译+link+RUN】
`cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl "$(FILE_NAME)" & echo Running: & "$(NAME_PART).exe" & PAUSE & EXIT`

### (2)TDM-GCC-32
#### 【Debug+RUN】
`cmd /k chdir /d "$(CURRENT_DIRECTORY)" & gcc -o "$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" & PAUSE & EXIT`
`cmd /k gcc -o "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" && CLS && "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" & PAUSE & EXIT`
***
## 3.扩展命令
```cmd
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & gcc -o "$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" & PAUSE & EXIT
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl/c "$(FILE_NAME)" & PAUSE & EXIT{编译但不link}
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & gcc -o "$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" & PAUSE & EXIT
cmd /k gcc -o "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" && CLS && "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" & PAUSE & EXIT
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl/ML "$(FILE_NAME)" & PAUSE & EXIT[编译以创建?线程可执行文件]
/MD 使用 MSVCRT.lib 编译以创建多线程 DLL 
/MDd 使用 MSVCRTD.lib 编译以创建调试多线程 DLL 
/ML 使用 LIBC.lib 编译以创建单线程可执行文件 
/MLd 使用 LIBCD.lib 编译以创建调试单线程可执行文件 
/MT 使用 LIBCMT.lib 编译以创建多线程可执行文件 
/MTd 使用 LIBCMTD.lib 编译以创建调试多线程可执行文件 
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl/? "$(FILE_NAME)" & PAUSE & EXIT
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & gcc -g -o "$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" & PAUSE & EXIT
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl/c "$(FILE_NAME)" & PAUSE & EXIT
```

## 4.软件
### (1).vc6.0++
[cv6.0++][1]
### (2).notepad++
[notepad++][2]
### (3).tdm-gcc
[tdm-gcc][3]

***
## 5.详解
### (1)vc6.0++
```cmd
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl "$(FILE_NAME)" & echo Running: & "$(NAME_PART).exe" & PAUSE & EXIT【编译+link+RUN】
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & cl "$(FILE_NAME)" & PAUSE & EXIT[编译+LINK]
cmd /k chdir /d "$(CURRENT_DIRECTORY)" & echo Running: & "$(NAME_PART).exe" & PAUSE & EXIT
```
### (2)tdm-gcc
```cmd
cmd /k gcc -Wall -o "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" & PAUSE & EXIT[exe]
cmd /k "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" & PAUSE & EXIT[run]
cmd /k 的含义是执行后面的命令，并且执行完毕后保留窗口。& 是连接多条命令。PAUSE 表示运行结束后暂停，等待一个任意按键。EXIT 表示关闭命令行窗口。如果使用 cmd /c 就可以省掉 EXIT 了。
关于脚本行：cmd /k gcc -o "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" && CLS && "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" & PAUSE & EXIT
cmd /k是弹出cmd窗口并执行后续指令。&和&&表示连接作用，说明有多行指令合为一行。
```
```
(1)gcc -o "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" "$(FULL_CURRENT_PATH)" 编译生成.exe文件，输出源文件所在目录。注意这里需要有==引号==，目的是为了在目录及文件名存在**空格**的情况下也能够正常运行。
(2)&& CLS 清屏。&&的意义在于，前面的语句出现错误，将不会执行该句。
(3)&& "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" 运行刚刚生成的.exe文件。如果先前的编译错误，将不会运行。
(4)& PAUSE 暂停，提示”按任意键继续“。这里无论前面有否错误，都将执行。因此我们看到的是编译错误，显示错误信息并暂停的窗口。
(5)& EXIT 退出。
```
***
$(FULL_CURRENT_PATH) 的含义是当前文件的完整路径，这是 Notepad++ 的宏定义，更多的相关宏可以参见官方FAQ：Run external tools。注意要用引号括起来，防止路径中间有空格。
直接执行 python.exe 在运行结束后窗口会自动关闭，所以要用 cmd 来执行。（在 Python 2.5 似乎有所不同，但是 3.0 需要这样设置。）
cmd /k 的含义是执行后面的命令，并且执行完毕后保留窗口。& 是连接多条命令。PAUSE 表示运行结束后暂停，等待一个任意按键。EXIT 表示关闭命令行窗口。如果使用 cmd /c 就可以省掉 EXIT 了。
(1) GCC for Windows通常使用两个版本，MinGW和Cygwin。文中的TDM-GCC是MinGW的一个版本。通用版MinGW和Cygwin不易安装，使用TDM-GCC或者Dev-Cpp自带的MinGW编译器会使得配置相对简单。
(2) gcc [附加选项] -o "输出文件" "源文件1" ["源文件2（可多个）"]是gcc命令行的标准模式。如
gcc -std=c99 -O2 -s -o "$(CURRENT_DIRECTORY)\$(NAME_PART).exe" "$(FULL_CURRENT_PATH)"意思就是以C99模式(-std=c99)，尽可能多地优化(-O2)，最小尺寸(-s)来编译。
同样地，C++也可以使用类似的命令行来编译。将gcc换为g++即可(注意-std=c99不可出现在C++中，另外文件的扩展名需注意更改为.cpp,.cc,.cxx,.C等)
头文件(.h)直接在源文件中书写如#include"system.h"即可，将自动查找源文件所在目录来获取.h文件。当然也可以使用-I "include目录"命令来添加非标准库。如此将不一一赘述。


  ***
 [文档HTML格式地址][4]


  [1]: https://download.microsoft.com/download/e/c/9/ec94a5d4-d0cf-4484-8b7a-21802f497309/Vs6sp6.exe
  [2]: https://github.com/baked-pan/c-relevant/blob/master/software/npp.6.9.2.Installer.exe?raw=true
  [3]: https://github.com/baked-pan/c-relevant/blob/master/software/tdm-gcc-5.1.0-3.exe?raw=true
  [4]: https://cdn.rawgit.com/baked-pan/c-relevant/master/compile/tdm-gcc-VC++6.0%E9%85%8D%E5%90%88notepad++%E7%BC%96%E8%AF%91%E8%BF%90%E8%A1%8CC%E6%96%87%E4%BB%B6.html